public class Pokemon {
    public String name;

    public Pokemon(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
