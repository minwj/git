public class AverageFinder {
    double computeAverage(int[] intArray) {
        double sum = 0;
        int indexSize = 0;
        double result;

        for (int i : intArray ){
            sum += i;
            indexSize += 1;
        }

        result = sum / indexSize;
        return result;

    }
}