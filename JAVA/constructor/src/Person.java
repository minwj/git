public class Person {
    private String name;
    private int age;
    private int cashAmount;

    BankAccount account;

    //게터세터


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setCashAmount(int cashAmount) {
        this.cashAmount = cashAmount;
    }

    public int getCashAmount() {
        return cashAmount;
    }

    public void setAccount(BankAccount account) {
        this.account = account;
    }

    public BankAccount getAccount() {
        return account;
    }

    //과제 생성자 2개

    public Person(String pName, int pAge) {
        name = pName;

        if (pAge < 0 ){
            age = 12;
        }else{
            age = pAge;
        }

        cashAmount = 0;
    }

    public Person(String pName, int pAge, int pCashAmount) {

        name = pName;

        if (pAge < 0 ){
            age = 12;
        }else{
            age = pAge;
        }

        if (pCashAmount < 0 ){
            cashAmount = 0;
        }else{
            cashAmount = pCashAmount;
        }
    }

    //Transfer

    public boolean transfer (Person to, int amount) {
        boolean success;

        if ((amount < 0) || (amount > account.getBalance())) {
            success = false;
        } else {
            account.setBalance( account.getBalance() - amount);
            to.getAccount().setBalance(to.getAccount().getBalance() + amount);
            success = true;
        }

        System.out.println(success + " - from: " + name
                + ", to: " + to.getName()
                + ", amount: " + amount
                + ", balance: " + account.getBalance());

        return success;

    }

    public boolean transfer (BankAccount to, int amount) {
        boolean success;

        if ((amount < 0) || (amount > account.getBalance())) {
            success = false;
        } else {
            account.setBalance( account.getBalance() - amount);
            to.setBalance(to.getBalance() + amount);
            success = true;
        }

        System.out.println(success + " - from: " + name
                + ", to: " + to.getOwner().getName()
                + ", amount: " + amount
                + ", balance: " + account.getBalance());

        return success;

    }

}
