public class BankAccount {
    private int balance;
    private Person owner;

    //게터 세터
    public int getBalance(){
        return balance;
    }

    public void setBalance(int newBalance) {
        balance = newBalance;
    }

    public Person getOwner(){
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }


    //과제 생성자 3개

    public BankAccount(int pBalance) {
        if (pBalance < 0){
            balance = 0;
        }else{
            balance = pBalance;
        }
    }

    public BankAccount(Person pOwner) {
        owner = pOwner;
        balance = 0;
    }

    public BankAccount(int pBalance, Person pOwner) {
        if (pBalance < 0){
            balance = 0;
        }else{
            balance = pBalance;
        }
        owner = pOwner;
    }


    //Transfer

    public boolean transfer (Person to, int amount) {
        boolean success;

        if ((amount < 0) || (amount > balance)) {
            success = false;
        } else {
            balance -= amount;
            to.getAccount().setBalance(to.getAccount().getBalance() + amount);
            success = true;
        }

        System.out.println(success + " - from: " + owner.getName()
                + ", to: " + to.getName()
                + ", amount: " + amount
                + ", balance: " + balance);

        return success;

    }

    public boolean transfer (BankAccount to, int amount) {
        boolean success;

        if ((amount < 0) || (amount > balance)) {
            success = false;
        } else {
            balance -= amount;
            to.setBalance(to.getBalance() + amount);
            success = true;
        }

        System.out.println(success + " - from: " + owner.getName()
                + ", to: " + to.getOwner().getName()
                + ", amount: " + amount
                + ", balance: " + balance);

        return success;

    }
}
