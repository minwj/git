public class Square implements Shape{
    public static final double PI = Math.PI;
    public final double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double getArea() {
        return side * side;
    }

    @Override
    public double getPerimeter() {
        return 4 *side;
}

}


