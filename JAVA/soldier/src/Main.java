import java.util.ArrayList;

public class Main {

    public static int getSurvivingIndex(int n, int k) {
        ArrayList<Integer> soldierList = new ArrayList<>();

        for(int i = 1; i <= n; i++){
            soldierList.add(i);
        }

        int kill = 0 ;

        while(soldierList.size() > 1){
                kill = (kill + k - 1) % soldierList.size() ;
            soldierList.remove(kill);

        }

        return soldierList.get(0);

    }

    public static void main(String[] args) {
        System.out.println("김신은 " + getSurvivingIndex(20, 5) + "번 자리에 서있으면 됩니다.");
    }

}
